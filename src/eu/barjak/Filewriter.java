/*
A reduced_elimexyyyyMMddHHmm.csv (pl. reduced_elimex201808231128.csv) néven 
fájlba írja, hogy hetenként össze lehessen hasonlítani az előzővel.
Formátuma cikkszám, elérhetőség:
...
LS-LDDDQ10B	a megrendeléstől számított 5-10 munkanap
LS-LDDDQ10SB	a megrendeléstől számított 5-10 munkanap
LS-LDDDQ12B	raktáron
LS-LDDDQ12SB	a megrendeléstől számított 5-10 munkanap
LS-LDDDQ15B	a megrendeléstől számított 5-10 munkanap
LS-LDDDQ15SB	a megrendeléstől számított 5-10 munkanap
LS-LDEB102G2B	raktáron
LS-LDEB102G2WB	raktáron
LS-LDEB102G3PC	a megrendeléstől számított 5-10 munkanap
...
*/

package eu.barjak;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Filewriter implements GlobalVariables {
    
    public void writeout() throws IOException {
        
        String out;
        String lf = "";

        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        String now = formatter.format(today);

        try (FileWriter fw = new FileWriter("reduced_elimex" + now + ".csv")) {
            for (HashMap.Entry<String, String> entry : R_NEWFILE.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                out = lf + key + ";" + value;
                lf = "\n";
                fw.write(out);
                //System.out.print(out);
            }
            //System.out.println();
        }
    }
}
