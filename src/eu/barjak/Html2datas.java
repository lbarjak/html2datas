package eu.barjak;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Html2datas {

    public static void main(String[] args) {
        try {
            new Elimex().html2map();
            new Filewriter().writeout();
            File oldFile = new File("reduced_elimex201808231128.csv");
            new Oldfile().readin(oldFile);
            //File newFile = new File("reduced_elimex201808231128.csv");
            //new Newfile().readin(newFile);//only test
            //new Comparision().compare();
            CompareHT compareHT = new CompareHT();
            compareHT.stockType();
            compareHT.compare();
        } catch (IOException ex) {
            Logger.getLogger(Html2datas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
