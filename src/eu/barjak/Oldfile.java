/*
Beolvassa a megadott régi fájlt, tartalmát beírja a R_OLDFILE-ba.
*/
package eu.barjak;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Oldfile implements GlobalVariables {
    
    public void readin(File oldFile) throws FileNotFoundException {
        
        Scanner newScanner = new Scanner(oldFile, StandardCharsets.UTF_8.name());

        while (newScanner.hasNext()) {
            String content = newScanner.useDelimiter("\n").next();
            //String[] contentRow = content.split(";");
            ArrayList<String> contRow = new ArrayList<>(Arrays.asList(content.split(";")));
            //contRow.addAll(Arrays.asList(contentRow));
            R_OLDFILE.put(contRow.get(0), contRow.get(1));
        }
//        R_OLDFILE.keySet().forEach((key)
//                -> System.out.println(key + ";" + R_OLDFILE.get(key))
//        );
    }
}
