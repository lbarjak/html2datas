/*
A Hangtechnika hangzavar áruházban használt elérhetőségekre cseréli az Elimex-nél
használtakat.
Az előző heti állapottal végez összehasonlítást.
*/
package eu.barjak;

import java.util.LinkedHashMap;

public class CompareHT implements GlobalVariables {
    
    public void stockType() {
        
        HANGTECH.put("raktáron", "1-2 nap");
        HANGTECH.put("date", "2-4 hét");
        HANGTECH.put("a megrendeléstől számított 1-5 munkanap", "1-5 munkanap");
        HANGTECH.put("a megrendeléstől számított 5-10 munkanap", "5-10 munkanap");
        HANGTECH.put("a megrendeléstől számított 10-15 munkanap", "2-4 hét");
        HANGTECH.put("a megrendeléstől számított 3-4 hét", "2-4 hét");
        HANGTECH.put("a megrendeléstől számított 4-5 hét", "2-4 hét");
        HANGTECH.put("-- átmeneti készlethiány --", "Jelenleg nem elérhető!");
        HANGTECH.put("Keressen a részletekért", "Hívjon!");
        HANGTECH.put("csak előzetes ajánlat alapján", "Hívjon!");
        HANGTECH.put("rendeléskor egyeztetjük", "Hívjon!");
        HANGTECH.put("Kifutó típus, már csak a készlet erejéig szállítható! raktáron", "1-2 nap");
        HANGTECH.put("Kifutó típus, már csak a készlet erejéig szállítható!", "Már nem szállítjuk");
        HANGTECH.put("Megszűnt! Már nem tudjuk szállítani!", "Már nem szállítjuk");
    }
    
    public void compare() {
        LinkedHashMap minus = (LinkedHashMap) R_OLDFILE.clone();
        R_NEWFILE.keySet().forEach((key) -> {
            minus.remove(key);
        });
        System.out.println("Megszűntek");
        minus.keySet().forEach((k) -> {
            System.out.println(k + ";" + minus.get(k));
        });

        LinkedHashMap plus = (LinkedHashMap) R_NEWFILE.clone();
        R_OLDFILE.keySet().forEach((key) -> {
            plus.remove(key);
        });
        System.out.println("\nÚjak");
        plus.keySet().forEach((k) -> {
            System.out.println(k + ";" + plus.get(k));
        });

        System.out.println("\nVáltoztak");
        String rx = "201\\d.\\d{2}.\\d{2}";
        R_NEWFILE.keySet().forEach((key) -> {
            if (R_NEWFILE.containsKey(key)
                    && R_OLDFILE.containsKey(key)
                    && !R_NEWFILE.get(key).equals(R_OLDFILE.get(key))) {
                System.out.println(
                        key + ";"
                        + HANGTECH.get(R_OLDFILE.get(key).replaceAll(rx, "date")) + ";"
                        + HANGTECH.get(R_NEWFILE.get(key).replaceAll(rx, "date")));
            }
        });
    }
}
