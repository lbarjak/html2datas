//R, azaz reduced, mivel a LinkedHashMap miatt nem tartalmaz ismétlődéseket
package eu.barjak;

import java.util.LinkedHashMap;

public interface GlobalVariables {
    
    LinkedHashMap<String, String> R_OLDFILE = new LinkedHashMap<>();
    LinkedHashMap<String, String> R_NEWFILE = new LinkedHashMap<>();
    LinkedHashMap<String, String> HANGTECH = new LinkedHashMap<>();
    
}
