/*
Csak teszteléshez, beolvassa a megadott új fájlt, tartalmát beírja a R_NEWFILE-ba.
*/
package eu.barjak;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Newfile implements GlobalVariables {
    
    public void readin(File newFile) throws FileNotFoundException {
        
        Scanner newScanner = new Scanner(newFile, StandardCharsets.UTF_8.name());

        while (newScanner.hasNext()) {
            String content = newScanner.useDelimiter("\n").next();
            ArrayList<String> contRow = new ArrayList<>(Arrays.asList(content.split(";")));
            R_NEWFILE.put(contRow.get(0), contRow.get(1));
        }
//        R_NEWFILE.keySet().forEach((key)
//                -> System.out.println(key + ";" + R_NEWFILE.get(key))
//        );
    }
}