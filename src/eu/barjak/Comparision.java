/*
Összehasonlítja a R_OLDFILE és a R_NEWFILE tartalmát, az eredmény a konzolra írja.
*/
package eu.barjak;

import java.util.LinkedHashMap;

public class Comparision implements GlobalVariables {

    public void compare() {

        LinkedHashMap<String, String> minus = new LinkedHashMap<>(R_OLDFILE);
        R_NEWFILE.keySet().forEach((key) -> minus.remove(key));
        System.out.println("Megszűntek");
        minus.keySet().forEach((key)
                -> System.out.println(key + ";" + minus.get(key))
        );

        LinkedHashMap<String, String> plus = new LinkedHashMap<>(R_NEWFILE);
        R_OLDFILE.keySet().forEach((key) -> plus.remove(key));
        System.out.println("\nÚjak");
        plus.keySet().forEach((key)
                -> System.out.println(key + ";" + plus.get(key))
        );

        System.out.println("\nVáltoztak");
        R_NEWFILE.keySet().forEach((key) -> {
            if (R_NEWFILE.containsKey(key)
                    && R_OLDFILE.containsKey(key)
                    && !R_NEWFILE.get(key).equals(R_OLDFILE.get(key))) {
                System.out.println(key
                        + ";" + R_OLDFILE.get(key)
                        + ";" + R_NEWFILE.get(key));
            }
        });
    }
}
