/*
Végignézi az Elimex oldalát, a html-ből kikeresi az adatokat és az R_NEWFILE-ba írja.
*/
package eu.barjak;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Elimex implements GlobalVariables {

    public void html2map() {

        int prod;
        int page;
        String match1;
        String match2 = "";
        String match3 = "";

        StringBuilder row = new StringBuilder();

        for (prod = 75; prod <= 1270; prod++) {//75, 1188, 1265
            for (page = 1; page < 12; page++) {
                try {
                    URL url = new URL("https://elimex.hu/gyartokategoriak/" + prod + "?list&page=" + page);
                    BufferedReader in;
                    try {
                        in = new BufferedReader(
                                new InputStreamReader(url.openStream()));
                        String inputLine;
                        
                        while ((inputLine = in.readLine()) != null) {
                            Pattern pattern1 = Pattern.compile(
                                    "(?<=>)[A-Z\\d-+]+(?=</a></span><b)");
                            Matcher matcher1 = pattern1.matcher(inputLine);
                            if (matcher1.find()) {
                                match1 = matcher1.group();
                                Pattern pattern2 = Pattern.compile(
                                        "raktáron"
                                                + "|201\\d.\\d{2}.\\d{2}"
                                                + "|a megrendeléstől számított 1-5 munkanap"
                                                + "|a megrendeléstől számított 5-10 munkanap"
                                                + "|a megrendeléstől számított 10-15 munkanap"
                                                + "|a megrendeléstől számított 3-4 hét"
                                                + "|a megrendeléstől számított 4-5 hét"
                                                + "|-- átmeneti készlethiány --"
                                                + "|Keressen a részletekért"
                                                + "|csak előzetes ajánlat alapján"
                                                + "|rendeléskor egyeztetjük"
                                                + "|Kifutó típus, már csak a készlet erejéig szállítható!"
                                                + "|Megszűnt! Már nem tudjuk szállítani!");
                                Matcher matcher2 = pattern2.matcher(inputLine);
                                if (matcher2.find()) {
                                    match2 = matcher2.group();
                                    if ("Kifutó típus, már csak a készlet erejéig szállítható!".equals(matcher2.group())) {
                                        Pattern pattern3 = Pattern.compile("raktáron");
                                        Matcher matcher3 = pattern3.matcher(inputLine);
                                        if (matcher3.find()) {
                                            match3 = matcher3.group();
                                        }
                                    }
                                }
                                row.append(match2);
                                if (!"".equals(match3)) {
                                    row.append(" ").append(match3);
                                    match3 = "";
                                }
                                R_NEWFILE.put(match1, row.toString());
                                row.setLength(0);
                                match2 = "";
                            }
                        }
                    } catch (IOException e) {
                        page = 12;
                        System.out.println((100 * prod / 1265) - 5 + "%");
                    }
                } catch (MalformedURLException ex) {
                    Logger.getLogger(Elimex.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
